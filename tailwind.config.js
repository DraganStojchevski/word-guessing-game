/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./*.html"],
  theme: {
    extend: {
      fontFamily: {
        jersey: ['"Jersey 15"', "sans-serif"],
        chakra: ['"Chakra Petch"', "sans-serif"],
      },
      keyframes: {
        fadeIn: {
          "0%": { opacity: 0 },
          "100%": { opacity: 1 },
        },
        pulse: {
          "0%, 100%": { transform: "scale(1)" },
          "50%": { transform: "scale(1.05)" },
        },
      },
      animation: {
        "fade-in": "fadeIn 0.5s ease-out",
        pulse: "pulse 2s infinite",
      },
    },
  },
  plugins: [],
};
