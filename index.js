document.addEventListener("DOMContentLoaded", function () {
  const notificationElement = document.getElementById("notification");
  const rulesSection = document.getElementById("rules-section");
  const continueQuery = document.getElementById("continue-query");
  const secondSection = document.getElementById("second-section");
  const yesButton = document.getElementById("yes-button");
  const noButton = document.getElementById("no-button");
  const newWordButton = document.getElementById("new-word-button");
  const continueButton = document.getElementById("continue-button");
  const categoryListElement = document.getElementById("category-list");

  class Wordy {
    constructor(categories) {
      this.categories = categories;
      this.currentCategory = null;
      this.currentWord = null;
      this.displayedWord = [];
      this.attemptsLeft = 0;
      this.guessesMade = new Set();
      this.notificationElement = notificationElement;
      this.populateCategoryList();
    }

    populateCategoryList() {
      categoryListElement.textContent = Object.keys(this.categories).join(", ");
    }

    showNotification(message) {
      this.notificationElement.textContent = message;
      this.notificationElement.classList.remove("hidden");
      this.notificationElement.classList.add("fade-in");
      setTimeout(() => {
        this.notificationElement.classList.add("hidden");
      }, 5000);
    }

    initializeGame() {
      const categoryNames = Object.keys(this.categories);
      this.currentCategory =
        categoryNames[Math.floor(Math.random() * categoryNames.length)];
      const words = this.categories[this.currentCategory];
      this.currentWord = words[Math.floor(Math.random() * words.length)]
        .toUpperCase()
        .trim();
      this.attemptsLeft = new Set(this.currentWord).size;
      this.displayedWord = Array.from(this.currentWord, (char) =>
        char === " " ? " " : "_"
      );
      this.guessesMade.clear();
      this.updateGameDisplay();
      secondSection.classList.remove("hidden");
      continueQuery.classList.add("hidden");
      rulesSection.classList.add("hidden");
    }

    updateGameDisplay() {
      const categoryElement = document.getElementById("category");
      categoryElement.textContent = `Category: ${this.currentCategory}`;
      const triesElement = document.getElementById("tries");
      triesElement.textContent = this.attemptsLeft;
      const wordDisplay = document.getElementById("word-display");
      wordDisplay.textContent = this.displayedWord.join(" ");
      const usedLettersElement = document.getElementById("used-letters");
      usedLettersElement.textContent = Array.from(this.guessesMade).join(", ");
    }

    playGame(event) {
      if (event.key.length === 1 && /[a-z]/i.test(event.key)) {
        const guess = event.key.toUpperCase();
        if (!this.guessesMade.has(guess)) {
          this.guessesMade.add(guess);
          let found = false;
          this.currentWord.split("").forEach((letter, index) => {
            if (letter === guess) {
              this.displayedWord[index] = letter;
              found = true;
            }
          });

          if (!found) {
            this.attemptsLeft--;
          }
          this.updateGameDisplay();

          if (!this.displayedWord.includes("_") && this.attemptsLeft > 0) {
            this.showNotification(
              "Congratulations! You've guessed the word correctly!"
            );
            this.askToContinue(true);
          } else if (this.attemptsLeft <= 0) {
            this.showNotification("You've run out of guesses!");
            this.askToContinue(false);
          }
        }
      } else if (event.key.length === 1 && /[^a-z]/i.test(event.key)) {
        this.showNotification("Only English keyboard input is allowed.");
      }
    }

    askToContinue(gameWon) {
      const continueMessage = document.getElementById("continue-message");
      if (gameWon) {
        continueMessage.innerHTML =
          "Well done! Would you like to guess another word?";
      } else {
        continueMessage.innerHTML = "Oops, wrong! Would you like to try again?";
      }
      secondSection.classList.add("hidden");
      continueQuery.classList.remove("hidden");
    }
  }

  const categories = {
    Animals: ["lion", "tiger", "elephant", "giraffe", "zebra"],
    Countries: ["France", "Brazil", "Japan", "Canada", "Italy"],
    Foods: ["pizza", "sushi", "taco", "pasta", "burger"],
    Occupations: ["doctor", "engineer", "teacher", "chef", "pilot"],
    Sports: ["soccer", "basketball", "tennis", "golf", "cricket"],
    Objects: ["chair", "lamp", "keyboard", "bottle", "phone"],
    "Historical Figures": ["Cleopatra", "Einstein", "Napoleon", "Gandhi"],
    "School Subjects": ["mathematics", "history", "biology", "art"],
    "Famous Landmarks": [
      "Eiffel Tower",
      "Great Wall of China",
      "Taj Mahal",
      "Statue of Liberty",
      "Colosseum",
    ],
    Furniture: ["sofa", "desk", "wardrobe", "bed", "bookshelf"],
    "Natural Phenomenons": [
      "tornado",
      "volcano",
      "earthquake",
      "tsunami",
      "hurricane",
    ],
    Weather: ["sunny", "cloudy", "rainy", "snowy", "stormy"],
    Apps: ["WhatsApp", "Instagram", "Snapchat", "TikTok", "Spotify"],
    Celebrities: ["Adele", "Drake", "Beyonce", "Rihanna", "Ed Sheeran"],
    Drinks: ["coffee", "tea", "soda", "water", "milk"],
    Clothes: ["jeans", "t shirt", "sweater", "dress", "jacket"],
    "Movie Types": ["comedy", "horror", "drama", "action", "documentary"],
    "Types Of Flowers": ["rose", "tulip", "daisy", "sunflower", "orchid"],
    Colors: ["red", "blue", "green", "yellow", "purple"],
    Cars: ["Tesla", "Toyota", "Ford", "BMW", "Audi"],
    "Music Genres": ["rock", "jazz", "classical", "pop", "hip hop"],
    "Family Tree": ["mother", "father", "sister", "brother", "cousin"],
  };

  const wordy = new Wordy(categories);

  document.addEventListener("keydown", function (event) {
    wordy.playGame(event);
  });

  continueButton.addEventListener("click", function () {
    wordy.initializeGame();
  });

  yesButton.addEventListener("click", function () {
    wordy.initializeGame();
  });

  noButton.addEventListener("click", function () {
    continueQuery.classList.add("hidden");
    rulesSection.classList.remove("hidden");
    wordy.showNotification("Thank you for playing!");
  });

  newWordButton.addEventListener("click", function () {
    wordy.initializeGame();
  });
});
