// const categories = {
//     Animals: ["lion", "tiger", "elephant", "giraffe", "zebra"],
//     Countries: ["France", "Brazil", "Japan", "Canada", "Italy"],
//     Foods: ["pizza", "sushi", "taco", "pasta", "burger"],
//     Occupations: ["doctor", "engineer", "teacher", "chef", "pilot"],
//     Sports: ["soccer", "basketball", "tennis", "golf", "cricket"],
//     Objects: ["chair", "lamp", "keyboard", "bottle", "phone"],
//     "Historical Figures": ["Cleopatra", "Einstein", "Napoleon", "Gandhi", "Joan of Arc"],
//     "School Subjects": ["mathematics", "history", "biology", "art", "computer science"],
//     "Famous Landmarks": ["Eiffel Tower", "Great Wall of China", "Taj Mahal", "Statue of Liberty", "Colosseum"],
//     Furniture: ["sofa", "desk", "wardrobe", "bed", "bookshelf"],
//     "Natural Phenomenons": ["tornado", "volcano", "earthquake", "tsunami", "hurricane"],
//     Weather: ["sunny", "cloudy", "rainy", "snowy", "stormy"],
//     Apps: ["WhatsApp", "Instagram", "Snapchat", "TikTok", "Spotify"],
//     Celebrities: ["Adele", "Drake", "Beyonce", "Rihanna", "Ed Sheeran"],
//     Drinks: ["coffee", "tea", "soda", "water", "milk"],
//     Clothes: ["jeans", "tshirt", "sweater", "dress", "jacket"],
//     "Movie Types": ["comedy", "horror", "drama", "action", "documentary"],
//     "Types Of Flowers": ["rose", "tulip", "daisy", "sunflower", "lily"],
//     Colors: ["red", "blue", "green", "yellow", "purple"],
//     Cars: ["Tesla", "Toyota", "Ford", "BMW", "Audi"],
//     "Music Genres": ["rock", "jazz", "classical", "pop", "hiphop"],
//     "Family Tree": ["mother", "father", "sister", "brother", "cousin"]
// };

// export { categories };
